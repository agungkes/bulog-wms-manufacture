interface BOMData {
  code: string;
  sh_bom_barcode_mobile: boolean;
  product_tmpl_id: [number, string] | boolean;
  state: string;
  display_name: string;
  bom_line_ids: number[];
  id: number;
  company_id: [number, string] | boolean;
  branch_ids: number[];
  picking_type_id: boolean;
  service_ids: number[];
  partner_id: boolean;
  active: boolean;
  cost_per_unit: number;
  type: string;
  service_product: boolean;
  message_ids: number[];
  approval_by_id: [number, string] | boolean;
  supplier_id: [number, string] | boolean;
  route_info_id: [number, string] | boolean;
  product_qty: number;
  subcontract_bom: boolean;
  sh_bom_bm_is_cont_scan: boolean;
  ready_to_produce: string;
  product_id: [number, string];
  sub_products: any[];
  message_follower_ids: number[];
  total_bom_cost: number;
  sequence: number;
  subcontract: boolean;
  branch_id: [number, string];
  product_uom_id: [number, string];
  routing_id: boolean;
}
interface BOMProduct {
  id: number;
  has_attachments: boolean;
  operation_id: boolean;
  sequence: number;
  total_cost: number;
  cost: number;
  product_qty: number;
  attribute_value_ids: any[];
  branch_id: Array<number | string>;
  product_id: Array<number | string>;
  product_uom_id: Array<number | string>;
}

interface ManufactoringOrderData {
  message_needaction: boolean;
  date_planned_finished: string;
  route_info_id: [number, string] | boolean;
  state: string;
  product_qty: number;
  name: string;
  availability: string;
  analytic_account_id: [number, string] | boolean;
  date_planned_start: string;
  id: number;
  origin: string;
  product_id: [number, string] | boolean;
  user_id: [number, string] | boolean;
  routing_id: boolean;
  product_uom_id: [number, string] | boolean;

  bom_id: [number, string] | boolean;
  company_id: [number, string] | boolean;
  branch_id: [number, string] | boolean;
  move_raw_ids?: number[];
  finished_move_line_ids?: number[];
}
interface ManufacturingLine {
  lots_visible: boolean;
  done_move: boolean;
  qty_done: number;
  branch_id: [number, string] | boolean;
  transfer_remark10: boolean;
  state: string;
  id: number;
  product_id: [number, string] | boolean;
  lot_id: [number, string] | boolean;
  product_uom_id: [number, string] | boolean;
}
