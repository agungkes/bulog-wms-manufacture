import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens, { appNavigation, authNavigation } from './src/screens';
import { useServerStore } from './src/stores/server.stores';

const App: ScreenFC = () => {
  const server = useServerStore();
  if (!server.getServer.sid) {
    authNavigation();
    return null;
  }

  if (server.getServer?.sid) {
    appNavigation();
    return null;
  }

  return null;
};

App.screenName = screens.INIT;
App.options = () => ({
  statusBar: {
    visible: false,
    drawBehind: true,
  },
  topBar: {
    visible: false,
    drawBehind: true,
    background: {
      color: '#02418b',
    },
    title: {
      color: '#fff',
    },
    backButton: {
      color: '#fff',
    },
  },
});

export default App;
