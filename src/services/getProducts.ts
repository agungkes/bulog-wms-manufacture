// import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getProducts = async () => {
  // const pagination = getObjectFromSearch(props);

  const data = await odoo.search_read<BOMData[]>('product.template', {
    context: { lang: 'en_US', tz: 'Asia/Jakarta' },
    domain: [['type', 'in', ['product', 'consu']]],
    fields: [
      'sequence',
      'default_code',
      'name',
      'list_price',
      'standard_price',
      'categ_id',
      'type',
      'qty_available',
      'virtual_available',
      'uom_id',
      'sh_qr_code',
    ],
    limit: 20,
  });

  return data;
};

export default getProducts;
