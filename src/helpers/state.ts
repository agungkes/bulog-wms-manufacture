const state: Record<string, string> = {
  draft: 'Draft',
  approved: 'Approved',
  approve: 'Approved',
  done: 'Done',
  cancel: 'Cancel',
};

export default state;
