const getObjectFromSearch = (props: string) => {
  const index = props.lastIndexOf('?');
  const searchString = props.slice(index + 1);

  return JSON.parse(
    '{"' +
      decodeURI(searchString)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/[=]/g, '":"') +
      '"}',
  );
};

export default getObjectFromSearch;
