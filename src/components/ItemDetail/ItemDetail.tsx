import React from 'react';
import { ListItem } from 'react-native-elements';

type ItemDetailProps = {
  title?: string;
  value?: string | number | [number, string] | boolean;
};
const ItemDetail: React.FC<ItemDetailProps> = ({ title, value }) => (
  <ListItem hasTVPreferredFocus tvParallaxProperties>
    <ListItem.Content>
      {title && <ListItem.Title>{title}</ListItem.Title>}
      <ListItem.Subtitle>
        {Array.isArray(value) ? value[1] : value}
      </ListItem.Subtitle>
    </ListItem.Content>
  </ListItem>
);

export default ItemDetail;
