import React from 'react';
import { View } from 'react-native';
import { ListItem } from 'react-native-elements';
import styles from './Item.styles';

type ItemProps = {
  title: string;
  value?: string | number | [number, string] | boolean;
};
const Item: React.FC<ItemProps> = ({ title, value }) => (
  <View style={styles.itemContainer}>
    <ListItem.Subtitle style={styles.itemLeftText}>{title}</ListItem.Subtitle>
    <ListItem.Title style={styles.itemRightText}>
      {Array.isArray(value) ? value[1] : value}
    </ListItem.Title>
  </View>
);

export default Item;
