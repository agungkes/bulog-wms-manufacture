import React from 'react';
import { ImageSourcePropType, Pressable, View } from 'react-native';
import DropShadow from 'react-native-drop-shadow';
import { Image, Text, useTheme } from 'react-native-elements';
import styles from './IconBox.styles';

interface Props {
  source: ImageSourcePropType;
  title: string;
  onPress?: () => void;
}

const IconBox: React.FC<Props> = ({ source, title, onPress }) => {
  const { theme } = useTheme();
  return (
    <DropShadow style={styles.dropShadow}>
      <View style={styles.container}>
        <Pressable
          android_ripple={{
            borderless: false,
            color: theme.colors?.accent,
          }}
          onPress={onPress}
          style={styles.button}>
          <Image source={source} style={styles.icon} />
          <Text style={styles.text}>{title}</Text>
        </Pressable>
      </View>
    </DropShadow>
  );
};

export default IconBox;
