import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  icon: {
    width: 50,
    height: 50,
    marginBottom: heightPercentageToDP(2),
  },
  dropShadow: {
    shadowColor: '#999999',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    width: '48%',
    marginBottom: heightPercentageToDP(2),
  },
  button: {
    padding: widthPercentageToDP(5),
    alignItems: 'center',
    borderRadius: widthPercentageToDP(5),
    minHeight: heightPercentageToDP(19),
  },
  text: {
    textAlign: 'center',
  },
});

export default styles;
