import React from 'react';
import { AppState, AppStateStatus } from 'react-native';
import { ThemeProvider } from 'react-native-elements';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { withNavigationProvider } from 'react-native-navigation-hooks/dist';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { SWRConfig } from 'swr';
import { ServerProvider } from '../../stores/server.stores';
import theme from '../../styles/theme';

const Provider = (Component: React.ComponentType<any>) =>
  withNavigationProvider((props: any) => {
    return (
      <SafeAreaProvider>
        <GestureHandlerRootView>
          <ThemeProvider theme={theme}>
            <ServerProvider>
              <SWRConfig
                value={{
                  fetcher: (resource, init) =>
                    fetch(resource, init).then(res => res.json()),
                  provider: () => new Map(),
                  isOnline() {
                    /* Customize the network state detector */
                    return true;
                  },
                  isVisible() {
                    /* Customize the visibility state detector */
                    return true;
                  },
                  initFocus(callback) {
                    /* Register the listener with your state provider */
                    let appState = AppState.currentState;

                    const onAppStateChange = (nextAppState: AppStateStatus) => {
                      /* If it's resuming from background or inactive mode to active one */
                      if (
                        appState.match(/inactive|background/) &&
                        nextAppState === 'active'
                      ) {
                        callback();
                      }
                      appState = nextAppState;
                    };

                    // Subscribe to the app state change events
                    const subscription = AppState.addEventListener(
                      'change',
                      onAppStateChange,
                    );

                    return () => {
                      subscription.remove();
                    };
                  },
                  // initReconnect(callback) {
                  //   /* Register the listener with your state provider */
                  // },
                }}>
                <Component {...props} />
              </SWRConfig>
            </ServerProvider>
          </ThemeProvider>
        </GestureHandlerRootView>
      </SafeAreaProvider>
    );
  });

export default Provider;
