import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  badge: {
    alignItems: 'center',
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#02418b',
    color: 'white',
  },
  badgeText: {
    color: 'white',
  },
  badgeInactive: {
    backgroundColor: '#e97e11',
  },
});

export default styles;
