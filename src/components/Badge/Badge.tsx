import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import styles from './Badge.styles';

interface Props {
  active: boolean;
}

const Badge = (props: Props) => {
  return (
    <View style={[styles.badge, !props.active && styles.badgeInactive]}>
      {props.active ? (
        <Text style={styles.badgeText}>Active</Text>
      ) : (
        <Text style={styles.badgeText}>Inactive</Text>
      )}
    </View>
  );
};

export default Badge;
