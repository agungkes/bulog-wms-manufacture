import React from 'react';
import { Alert, View } from 'react-native';
import DropShadow from 'react-native-drop-shadow';
import { Button, Input, useTheme } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import Icon from 'react-native-vector-icons/Ionicons';

import styles from './LoginForm.styles';
import { Controller, useForm } from 'react-hook-form';
import { object, string } from 'yup';

import { yupResolver } from '@hookform/resolvers/yup';
import { useServerStore } from '@stores/server.stores';
import useToggle from '@hooks/useToggle';
import useLoadingScreen from 'hooks/useLoadingScreen';
import { appNavigation } from 'screens';
import odoo from 'services/odoo.client';

const schema = object({
  email: string().required().defined(),
  password: string().required().defined(),
})
  .required()
  .defined();

type IFormData = {
  email: string;
  password: string;
};
const LoginForm: React.FC = () => {
  const { theme } = useTheme();
  const [showLoading, dismissLoading] = useLoadingScreen();
  const [showPassword, setShowPassword] = useToggle();
  const server = useServerStore();

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<IFormData>({
    resolver: yupResolver(schema),
    defaultValues: {
      email: '',
      password: '',
    },
  });

  const onSubmit = async (values: IFormData) => {
    try {
      await showLoading();
      if (!server.getServer.protocol) {
        throw new Error('Silahkan set protokol terlebih dahulu');
      }
      if (!server.getServer.database) {
        throw new Error('Belum ada database yang dipilih');
      }
      if (!server.getServer.domain) {
        throw new Error('Belum ada domain yang dipilih');
      }

      await odoo.connect({
        protocol: server.getServer.protocol,
        db: server.getServer.database,
        host: server.getServer.domain,
        password: values.password,
        username: values.email,
      });
      await dismissLoading();
      appNavigation();
    } catch (error: any) {
      await dismissLoading();

      Alert.alert('Error', error.message);
      console.log(error);
    }
  };

  return (
    <DropShadow style={styles.dropShadow}>
      <View style={styles.authContainer}>
        <Controller
          name="email"
          control={control}
          rules={{ required: true }}
          render={({ field: { onChange, onBlur, value } }) => (
            <Input
              autoCompleteType="email"
              keyboardType="email-address"
              returnKeyType="next"
              textContentType="emailAddress"
              label="Email"
              onChangeText={onChange}
              onBlur={onBlur}
              value={value}
              errorMessage={errors.email && 'Harus diisi'}
              autoCapitalize="none"
            />
          )}
        />

        <Controller
          name="password"
          control={control}
          rules={{ required: true }}
          render={({ field: { onChange, onBlur, value } }) => (
            <Input
              autoCompleteType="password"
              returnKeyType="go"
              textContentType="password"
              secureTextEntry={!showPassword}
              label="Password"
              rightIcon={
                !showPassword ? (
                  <Icon
                    name="eye"
                    size={widthPercentageToDP(7)}
                    onPress={setShowPassword}
                    color={'#000'}
                  />
                ) : (
                  <Icon
                    name="eye-off"
                    size={widthPercentageToDP(7)}
                    onPress={setShowPassword}
                    color={'#000'}
                  />
                )
              }
              onChangeText={onChange}
              onBlur={onBlur}
              value={value}
              errorMessage={errors.email && 'Kata sandi harus diisi'}
            />
          )}
        />

        <Button
          onPress={handleSubmit(onSubmit)}
          ViewComponent={LinearGradient}
          buttonStyle={{
            borderRadius: widthPercentageToDP(10),
            height: heightPercentageToDP(6.5),
          }}
          title={'LOGIN'}
          linearGradientProps={{
            colors: ['#0074f1', theme.colors?.primary],
            start: { x: 0, y: 0.5 },
            end: { x: 0.3, y: 0.5 },
          }}
        />
      </View>
    </DropShadow>
  );
};

export default LoginForm;
