type AuthenticateService = {
  protocol: string;
  host: string;
  database: string;
  username: string;
  password: string;
};

const authenticateService = async ({
  protocol,
  host,
  database,
  username,
  password,
}: AuthenticateService) => {
  try {
    const params = {
      db: database,
      login: username,
      password: password,
    };
    const json = JSON.stringify({ params: params });
    const url = `${protocol}://${host}/web/session/authenticate`;
    const res = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'Content-Length': `${json.length}`,
      },
      body: json,
      credentials: 'omit',
    });
    const response = await res.json();

    if (response.error) {
      throw new Error(response.error);
    }

    return {
      uid: response.result.uid,
      session_id: response.result.session_id,
      context: response.result.user_context,
      username: response.result.username,
    };
  } catch (error: any) {
    console.log(error.message);
  }
};

export default authenticateService;
