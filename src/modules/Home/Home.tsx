import Layout from '@components/Layout';
import IconBox from 'components/IconBox';
import React from 'react';
import { View } from 'react-native';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';

import styles from './Home.styles';
import screens, { authNavigation } from 'screens';
import { useNavigation } from 'react-native-navigation-hooks/dist';
import { storeServer } from 'stores/server.stores';
import useToggle from 'hooks/useToggle';
import { Button, Overlay, Text } from 'react-native-elements';

const Home: ScreenFC = () => {
  const navigation = useNavigation();

  const [showLogoutConfirmation, toggleLogoutConfirmation] = useToggle();

  const handleGoToMaterial = () => {
    navigation.push(screens.BILL_OF_MATERIAL);
  };
  const handleGoToManufacturingOrder = () => {
    navigation.push(screens.MANUFACTURING_ORDER_LIST);
  };
  const handleGoToScanQR = () => {
    navigation.push(screens.SCAN_QR);
  };

  const handleLogout = async () => {
    await storeServer.clearStoredDate();
    authNavigation();
  };
  return (
    <Layout>
      <View style={styles.container}>
        <IconBox
          source={require('../../assets/images/scan_code.png')}
          title="Scan QR Code"
          onPress={handleGoToScanQR}
        />
        <IconBox
          source={require('../../assets/images/bill.png')}
          title="Bill Of Material"
          onPress={handleGoToMaterial}
        />
        <IconBox
          source={require('../../assets/images/weight.png')}
          title="Manufacturing Order"
          onPress={handleGoToManufacturingOrder}
        />
        <IconBox
          source={require('../../assets/images/logout.png')}
          title="Logout"
          onPress={toggleLogoutConfirmation}
        />
      </View>

      <Overlay isVisible={showLogoutConfirmation}>
        <Text style={styles.logout}>Apakah anda yakin?</Text>
        <Button title="Ya" onPress={handleLogout} />
      </Overlay>
    </Layout>
  );
};

Home.screenName = screens.HOME;
Home.options = () => ({
  topBar: {
    title: {
      text: 'Bulog WMS Manufacture',
    },
  },
});
export default Home;
