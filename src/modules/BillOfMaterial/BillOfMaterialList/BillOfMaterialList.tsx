import Layout from 'components/Layout';
import React from 'react';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './BillOfMaterialList.styles';
import {
  useNavigation,
  // useNavigationButtonPress,
} from 'react-native-navigation-hooks';

import { FlatList } from 'react-native';

import useBillOfMaterialList from 'modules/BillOfMaterial/hooks/useBillOfMaterialList';
import FlatListEmpty from 'components/FlatListEmpty';
import Loading from 'components/Loading';
import BillOfMaterialItem from 'modules/BillOfMaterial/components/BillOfMaterialItem';

// import Icon from 'react-native-vector-icons/Ionicons';

type Props = {};

// const AddPerawatanGudangIcon = Icon.getImageSourceSync('md-add', 30, 'white');
const BillOfMaterialList: ScreenFC<Props> = () => {
  const navigation = useNavigation();
  const { data, loading, size, setSize } = useBillOfMaterialList();

  // const handleGoToCreate = () => {
  //   navigation.push(screens.BILL_OF_MATERIAL_CREATE);
  // };
  // useNavigationButtonPress(handleGoToCreate, { componentId: 'ADD_BOM' });

  const handleGoToDetail = (item: BOMData) => () => {
    navigation.push(screens.BILL_OF_MATERIAL_DETAIL, item);
  };
  const renderItem = ({ item }: { item: BOMData }) => {
    return <BillOfMaterialItem {...item} onPress={handleGoToDetail(item)} />;
  };
  const keyExtractor = (item: BOMData, idx: number) => `${item.id}__${idx}`;

  const onEndReached = () => {
    setSize(size + 1);
  };

  return (
    <Layout>
      <FlatList
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        data={[...data.flat(1)]}
        contentContainerStyle={styles.container}
        alwaysBounceVertical
        scrollEventThrottle={19}
        onEndReached={onEndReached}
        ListEmptyComponent={FlatListEmpty}
        removeClippedSubviews
        maxToRenderPerBatch={10}
        updateCellsBatchingPeriod={20}
        initialNumToRender={10}
      />

      <Loading isVisible={loading} />
    </Layout>
  );
};

BillOfMaterialList.screenName = screens.BILL_OF_MATERIAL;
BillOfMaterialList.options = () => ({
  topBar: {
    title: {
      text: 'Billing Of Material',
    },
  },
  // fab: {
  //   id: 'ADD_BOM',
  //   iconColor: '#fff',
  //   clickColor: '#e97e11',
  //   backgroundColor: '#02418b',
  //   icon: AddPerawatanGudangIcon,
  // },
});
export default BillOfMaterialList;
