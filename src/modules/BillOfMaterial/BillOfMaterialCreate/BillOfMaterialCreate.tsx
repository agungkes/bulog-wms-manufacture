import React from 'react';
import Layout from 'components/Layout';
import { Input } from 'react-native-elements';
import styles from './BillOfMaterialCreate.styles';
import { ScrollView } from 'react-native-gesture-handler';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';

// import Autocomplete from 'react-native-autocomplete-input';

const BillOfMaterialCreate: ScreenFC = () => {
  return (
    <Layout>
      <ScrollView
        contentContainerStyle={styles.container}
        keyboardShouldPersistTaps="always">
        <Input autoCompleteType label="Product" editable={false} />
        <Input autoCompleteType label="Quantity to Product" />
        <Input autoCompleteType label="Bill Of Material" />
        <Input autoCompleteType label="Deadline Start" />
        <Input autoCompleteType label="Responsible" />
        <Input autoCompleteType label="Source" />
      </ScrollView>

      {/* <Overlay
        isVisible={showProductSelection}
        onBackdropPress={toggleProductSelection}
        hardwareAccelerated
        fullScreen>
        <Autocomplete
          data={products}
          // value={}
          autoFocus
          onChangeText={handleChangeSearchProduct}
          flatListProps={{
            keyExtractor: (_, idx) => idx,
            renderItem: ({ item }) => <Text>{item}</Text>,
          }}
          style={{
            color: 'red',
          }}
        />
      </Overlay> */}
    </Layout>
  );
};

BillOfMaterialCreate.screenName = screens.BILL_OF_MATERIAL_CREATE;
BillOfMaterialCreate.options = () => {
  return {
    topBar: {
      title: {
        text: 'Bill Of Material',
      },
    },
  };
};
export default BillOfMaterialCreate;
