import Item from 'components/Item';
import state from 'helpers/state';
import React from 'react';
import { View } from 'react-native';
import DropShadow from 'react-native-drop-shadow';
import { Divider, ListItem, Text } from 'react-native-elements';
import styles from './BillOfMaterialItem.styles';

type Props = BOMData & {
  onPress?: () => void;
};

const BillOfMaterialItem: React.FC<Props> = ({ onPress, ...props }) => {
  return (
    <DropShadow style={styles.container}>
      <ListItem onPress={onPress} hasTVPreferredFocus tvParallaxProperties>
        <ListItem.Content>
          <View style={styles.header}>
            <Text style={styles.headerTextSurat}>{props.code}</Text>
            <Text style={styles.headerTextDate}>{state[props.state]}</Text>
          </View>
          <Divider style={styles.divider} />

          <Item title="Quantity" value={props.product_qty} />
          <Item title="Product UOM" value={props.product_uom_id} />
          <Item title="Rute" value={props.route_info_id} />
        </ListItem.Content>
      </ListItem>
    </DropShadow>
  );
};

export default BillOfMaterialItem;
