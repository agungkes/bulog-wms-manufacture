import BillOfMaterialList from './BillOfMaterialList';
import BillOfMaterialDetail from './BillOfMaterialDetail';

export { BillOfMaterialList, BillOfMaterialDetail };
