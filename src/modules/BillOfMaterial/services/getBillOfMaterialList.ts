// import getStockPickingType from 'services/getStockPickingType';
import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getBillOfMaterialList = async (props: string) => {
  const pagination = getObjectFromSearch(props);

  const data = await odoo.search_read<BOMData[]>('mrp.bom', {
    context: { lang: 'en_US', tz: 'Asia/Jakarta' },
    fields: [
      'state',
      'approval_by_id',
      'active',
      'product_tmpl_id',
      'product_id',
      'product_qty',
      'product_uom_id',
      'routing_id',
      'route_info_id',
      'code',
      'type',
      'subcontract_bom',
      'supplier_id',
      'company_id',
      'branch_id',
      'branch_ids',
      'sh_bom_barcode_mobile',
      'sh_bom_bm_is_cont_scan',
      'bom_line_ids',
      'sequence',
      'ready_to_produce',
      'picking_type_id',
      'service_ids',
      'sub_products',
      'subcontract',
      'partner_id',
      'service_product',
      'cost_per_unit',
      'message_follower_ids',
      'message_ids',
      'display_name',
    ],
    limit: Number(pagination.limit),
    offset: Number(pagination.offset),
  });

  return data;
};

export default getBillOfMaterialList;
