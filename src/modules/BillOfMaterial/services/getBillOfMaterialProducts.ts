import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getBillOfMaterialProducts = async (props: string) => {
  const url = getObjectFromSearch(props);
  const ids = url.ids.split(',').map((e: string) => Number(e));
  const data = await odoo.get<BOMProduct[]>('mrp.bom.line', {
    args: [
      ids,
      [
        'sequence',
        'product_id',
        'branch_id',
        'has_attachments',
        'product_qty',
        'product_uom_id',
        'cost',
        'total_cost',
        'attribute_value_ids',
        'operation_id',
      ],
    ],
    kwargs: {},
  });

  return data || [];
};

export default getBillOfMaterialProducts;
