import { getBillOfMaterialList } from 'modules/BillOfMaterial/services';
import useSWRInfinite from 'swr/infinite';

const getKey = (pageIndex: number, previousPageData: any) => {
  if (previousPageData && !previousPageData.length) {
    return null;
  } // reached the end

  return `/api/get-bill-of-material-list?page=${Number(
    pageIndex,
  )}&limit=10&offset=${pageIndex * 10}`;
};

const usePickingList = () => {
  const { data, size, setSize, error, ...rest } = useSWRInfinite(
    getKey,
    getBillOfMaterialList,
  );

  const isLoadingInitialData = !data && !error;
  const isLoadingMore =
    isLoadingInitialData ||
    (size > 0 && data && typeof data[size - 1] === 'undefined');

  return {
    data: data || [],
    loading: isLoadingMore || isLoadingInitialData,
    size,
    setSize,
    ...rest,
  };
};
export default usePickingList;
