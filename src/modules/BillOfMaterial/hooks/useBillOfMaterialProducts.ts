// import { getPickingProducts } from 'modules/Picking/services';
import { getBillOfMaterialProducts } from 'modules/BillOfMaterial/services';
import useSWR from 'swr';

const useBillOfMaterialProducts = (ids: number | number[]) => {
  const { data, ...rest } = useSWR(
    `/api/bom/products/?ids=${ids}`,
    getBillOfMaterialProducts,
  );

  return {
    ...rest,
    data: data || [],
    loading: !data,
  };
};

export default useBillOfMaterialProducts;
