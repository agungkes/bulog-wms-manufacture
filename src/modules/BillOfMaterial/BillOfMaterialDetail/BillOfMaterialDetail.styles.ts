import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    padding: widthPercentageToDP(4),
  },
  detailContainer: {
    marginBottom: heightPercentageToDP(2),
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {},
  detail: {
    marginVertical: heightPercentageToDP(1),
  },

  badge: {
    alignItems: 'center',
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#02418b',
    color: 'white',
  },
  badgeText: {
    color: 'white',
  },
  badgeInactive: {
    backgroundColor: '#e97e11',
  },

  productContainer: {
    flexDirection: 'row',
  },
});

export default styles;
