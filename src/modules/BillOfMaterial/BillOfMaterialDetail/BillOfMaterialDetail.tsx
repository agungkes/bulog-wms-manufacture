import Badge from 'components/Badge';
import ItemDetail from 'components/ItemDetail';
import Layout from 'components/Layout';
import Loading from 'components/Loading';
import useBillOfMaterialProducts from 'modules/BillOfMaterial/hooks/useBillOfMaterialProducts';
import React from 'react';
import { View } from 'react-native';
import { ListItem, Text } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './BillOfMaterialDetail.styles';

export type BillOfMaterialDetailProps = BOMData & {};
const BillOfMaterialDetail: ScreenFC<BillOfMaterialDetailProps> = props => {
  const { data: products = [], loading } = useBillOfMaterialProducts(
    props.bom_line_ids,
  );

  return (
    <Layout>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            {props.code}
          </Text>
        </View>

        <View style={styles.detail}>
          <ItemDetail title="Product Jadi" value={props.product_id} />
          <ItemDetail title="Product Variant" value={props.product_tmpl_id} />
          <ItemDetail
            title="Quantity"
            value={`${props.product_qty} ${
              props.product_uom_id ? props.product_uom_id[1] : ''
            }`}
          />
          <ItemDetail title="Reference" value={props.code} />
          <ItemDetail title="Route" value={props.route_info_id} />
          <ItemDetail title="Supplier" value={props.supplier_id} />
          <ItemDetail title="Company" value={props.company_id} />
          <ItemDetail title="Business Unit" value={props.branch_id} />
        </View>

        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            Products
          </Text>
        </View>

        <View style={styles.detail}>
          {products.map(product => (
            <ListItem
              bottomDivider
              key={product.id}
              hasTVPreferredFocus
              tvParallaxProperties>
              <ListItem.Content>
                <ListItem.Title>{product.product_id[1]}</ListItem.Title>
                <ListItem.Subtitle>
                  Branch: {product.branch_id ? product.branch_id[1] : ''}
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                  Quantity: {product.product_qty}
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                  Unit of Measure:{' '}
                  {product.product_uom_id ? product.product_uom_id[1] : ''}
                </ListItem.Subtitle>
                <ListItem.Subtitle>Cost: {product.cost}</ListItem.Subtitle>
                <ListItem.Subtitle>
                  Total Cost: {product.total_cost}
                </ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>
          ))}
        </View>

        <Badge active={props.active} />
      </ScrollView>
      <Loading isVisible={loading} />
    </Layout>
  );
};

BillOfMaterialDetail.screenName = screens.BILL_OF_MATERIAL_DETAIL;
BillOfMaterialDetail.options = props => {
  return {
    topBar: {
      drawBehind: false,
      visible: true,
      title: {
        text: props.code,
      },
    },
  };
};
export default BillOfMaterialDetail;
