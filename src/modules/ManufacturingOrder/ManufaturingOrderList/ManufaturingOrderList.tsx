import React from 'react';
import Layout from 'components/Layout';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './ManufaturingOrderList.styles';
import { FlatList } from 'react-native-gesture-handler';

import ManufacturingOrderItem from '../components/ManufacturingOrderItem';
import { useNavigation } from 'react-native-navigation-hooks';
import useManufacturingList from 'modules/ManufacturingOrder/hooks/useManufacturingList';
import FlatListEmpty from 'components/FlatListEmpty';
import Loading from 'components/Loading';

const ManufaturingOrderList: ScreenFC = () => {
  const navigation = useNavigation();

  const { data, loading, size, setSize } = useManufacturingList();

  const handleGoToDetail = (item: ManufactoringOrderData) => () => {
    navigation.push(screens.MANUFACTURING_ORDER_DETAIL, item);
  };
  const renderItem = ({ item }: { item: ManufactoringOrderData }) => {
    return (
      <ManufacturingOrderItem {...item} onPress={handleGoToDetail(item)} />
    );
  };
  const keyExtractor = (item: ManufactoringOrderData) => `${item.id}`;
  const onEndReached = () => {
    setSize(size + 1);
  };
  return (
    <Layout>
      <FlatList
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        data={data.flat(1) as unknown as ManufactoringOrderData[]}
        contentContainerStyle={styles.container}
        scrollEventThrottle={19}
        onEndReached={onEndReached}
        ListEmptyComponent={FlatListEmpty}
        removeClippedSubviews
        maxToRenderPerBatch={10}
        updateCellsBatchingPeriod={20}
        initialNumToRender={10}
      />
      <Loading isVisible={loading} />
    </Layout>
  );
};

ManufaturingOrderList.screenName = screens.MANUFACTURING_ORDER_LIST;
ManufaturingOrderList.options = () => {
  return {
    topBar: {
      drawBehind: false,
      visible: true,
      title: {
        text: 'Manufacturing Order',
      },
    },
  };
};
export default ManufaturingOrderList;
