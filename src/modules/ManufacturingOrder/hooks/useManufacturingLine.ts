import { getManufacturingLine } from 'modules/ManufacturingOrder/services';
import useSWR from 'swr';

const useManufacturingLine = (ids?: number | number[]) => {
  const { data, ...rest } = useSWR<ManufacturingLine[]>(
    ids ? `/api/get-manufacture-order-line?ids=${ids}` : null,
    getManufacturingLine,
  );

  return {
    ...rest,
    data: data || [],
    loading: !data,
  };
};

export default useManufacturingLine;
