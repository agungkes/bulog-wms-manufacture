import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getManufacturingLine = async (key: string) => {
  const { ids } = getObjectFromSearch(key);
  const searchIds = ids.split(',').map((e: string) => Number(e));

  const data = await odoo.get<ManufacturingLine[]>('stock.move.line', {
    args: [
      searchIds,
      [
        'product_id',
        'branch_id',
        'lot_id',
        'transfer_remark10',
        'product_uom_id',
        'qty_done',
        'lots_visible',
        'done_move',
        'state',
      ],
    ],
    kwargs: {},
  });

  return data || [];
};

export default getManufacturingLine;
