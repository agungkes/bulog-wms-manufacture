import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getManufacturingList = async (props: string) => {
  const pagination = getObjectFromSearch(props);

  const data = await odoo.search_read<any[]>('mrp.production', {
    fields: [
      'display_name',
      'name',
      'message_needaction',
      'name',
      'date_planned_start',
      'product_id',
      'product_qty',
      'product_uom_id',
      'analytic_account_id',
      'availability',
      'routing_id',
      'route_info_id',
      'origin',
      'state',
      'user_id',
      'date_planned_start',
      'date_planned_finished',
      'bom_id',
      'company_id',
      'branch_id',
      'move_raw_ids',
      'finished_move_line_ids',
    ],
    domain: [['state', 'in', ['draft', 'confirmed', 'planned', 'progress']]],
    limit: Number(pagination.limit),
    offset: Number(pagination.offset),
    sort: 'date_planned_start DESC',
  });

  return data || [];
};

export default getManufacturingList;
