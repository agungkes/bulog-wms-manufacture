import Item from 'components/Item';
import React from 'react';
import { View } from 'react-native';
import DropShadow from 'react-native-drop-shadow';
import { Divider, ListItem, Text } from 'react-native-elements';
import styles from './ManufacturingOrderItem.styles';

type ManufacturingOrderItemProps = ManufactoringOrderData & {
  onPress?: () => void;
};
const ManufacturingOrderItem: React.FC<ManufacturingOrderItemProps> = ({
  onPress,
  ...props
}) => {
  return (
    <DropShadow style={styles.container}>
      <ListItem hasTVPreferredFocus tvParallaxProperties onPress={onPress}>
        <ListItem.Content>
          <View style={styles.header}>
            <Text style={styles.headerTextSurat}>{props.name}</Text>
            <Text style={styles.headerTextDate}>{props.state}</Text>
          </View>
          <Divider style={styles.divider} />

          <Item title="Product" value={props.product_id} />
          <Item title="Quantity to Product" value={props.product_qty} />
          <Item title="Unit of Measure" value={props.product_uom_id} />
          <Item title="Rute" value={props.route_info_id} />
          <Item title="Source" value={props.analytic_account_id} />
        </ListItem.Content>
      </ListItem>
    </DropShadow>
  );
};

export default ManufacturingOrderItem;
