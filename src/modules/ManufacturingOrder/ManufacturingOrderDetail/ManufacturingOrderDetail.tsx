import ItemDetail from 'components/ItemDetail';
import Layout from 'components/Layout';
import Loading from 'components/Loading';
import useManufacturingLine from 'modules/ManufacturingOrder/hooks/useManufacturingLine';
import React from 'react';
import { View } from 'react-native';
import { ListItem, Text } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './ManufacturingOrderDetail.styles';

type ManufactoringOrderDetailProps = ManufactoringOrderData & {};
const ManufacturingOrderDetail: ScreenFC<
  ManufactoringOrderDetailProps
> = props => {
  const { data, loading } = useManufacturingLine(props.finished_move_line_ids);

  return (
    <Layout>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            {props.name}
          </Text>
        </View>

        <View style={styles.detail}>
          <ItemDetail title="Product" value={props.product_id} />
          <ItemDetail title="Quantity to Product" value={props.product_qty} />
          <ItemDetail title="Bill Of Material" value={props.bom_id} />
          <ItemDetail title="Deadline Start" value={props.date_planned_start} />
          <ItemDetail
            title="Deadline Finished"
            value={props.date_planned_finished}
          />
          <ItemDetail title="Responsible" value={props.user_id} />
          <ItemDetail
            title="Source Analytic Account"
            value={props.analytic_account_id}
          />
        </View>

        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            Products
          </Text>
        </View>
        <View style={styles.detail}>
          {data.map((product: ManufacturingLine) => (
            <ListItem
              bottomDivider
              key={product.id}
              hasTVPreferredFocus
              tvParallaxProperties>
              <ListItem.Content>
                <ListItem.Title>{product.product_id}</ListItem.Title>
                <ListItem.Subtitle>
                  Quantity: {product.qty_done}
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                  Unit of Measure: {product.product_uom_id}
                </ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>
          ))}
        </View>
      </ScrollView>

      <Loading isVisible={loading} />
    </Layout>
  );
};

ManufacturingOrderDetail.screenName = screens.MANUFACTURING_ORDER_DETAIL;

ManufacturingOrderDetail.options = props => {
  return {
    topBar: {
      drawBehind: false,
      visible: true,
      title: {
        text: props.name,
      },
    },
  };
};
export default ManufacturingOrderDetail;
