import ManufaturingOrderList from './ManufaturingOrderList';
import ManufacturingOrderDetail from './ManufacturingOrderDetail';

export { ManufaturingOrderList, ManufacturingOrderDetail };
