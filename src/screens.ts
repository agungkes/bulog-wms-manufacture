import { Navigation } from 'react-native-navigation';

const screens = {
  INIT: 'INIT',
  AUTHENTICATION: 'Authentication',
  HOME: 'Home',
  LOADING: 'Loading',
  BILL_OF_MATERIAL: 'BILL_OF_MATERIAL',
  BILL_OF_MATERIAL_CREATE: 'BILL_OF_MATERIAL_CREATE',
  BILL_OF_MATERIAL_DETAIL: 'BILL_OF_MATERIAL_DETAIL',

  MANUFACTURING_ORDER_LIST: 'MANUFACTURING_ORDER_LIST',
  MANUFACTURING_ORDER_DETAIL: 'MANUFACTURING_ORDER_DETAIL',

  SCAN_QR: 'SCAN_QR',
};

export default screens;

export const appNavigation = () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: screens.HOME,
            },
          },
        ],
      },
    },
  });
};

export const authNavigation = () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: screens.AUTHENTICATION,
            },
          },
        ],
      },
    },
  });
};
