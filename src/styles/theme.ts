import { Theme } from 'react-native-elements';
import { color } from 'react-native-elements/dist/helpers';
import { TextInput } from 'react-native-gesture-handler';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import converDpToPixel from '../helpers/convertDpToPixel';

const theme: Theme = {
  colors: {
    ...color,
    primary: '#02418b',
    accent: '#e97e11',
    blue: '#02418b',
  },
  Input: {
    // @ts-ignore
    InputComponent: TextInput,
    inputContainerStyle: {
      borderBottomWidth: 2,
    },
    containerStyle: {
      paddingHorizontal: 0,
    },
    labelStyle: {
      color: '#e97e11',
      fontWeight: 'normal',
      fontSize: widthPercentageToDP(converDpToPixel(14)),
    },
    style: {
      fontSize: widthPercentageToDP(converDpToPixel(16)),
    },
    selectionColor: '#e97e11',
  },
  Button: {},
};

export default theme;
