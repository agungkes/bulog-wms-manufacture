// import { getPickingProducts } from 'modules/Picking/services';
import { getBillOfMaterialProducts } from 'modules/BillOfMaterial/services';
import useSWR from 'swr';

const useProducts = (ids: number | number[]) => {
  const { data, ...rest } = useSWR(
    `/api/products/?name=${ids}`,
    getBillOfMaterialProducts,
  );

  return {
    ...rest,
    data: data || [],
    loading: !data,
  };
};

export default useProducts;
