module.exports = {
  root: true,
  extends: ['@react-native-community', 'plugin:import/typescript'],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'module-resolver'],
  rules: {
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': ['error'],
  },
  settings: {
    'import/resolver': {
      'babel-module': {
        '@components': ['./components/'],
        '@modules': ['./modules/'],
        '@services': ['./services/'],
        '@stores': ['./stores/'],
        '@hooks': ['./hooks/'],
        '@helpers': ['./helpers/'],
      },
    },
  },
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      rules: {
        'no-undef': 'off',
      },
    },
  ],
};
